package com.hendisantika.springbootjasper2.report;

import net.sf.jasperreports.engine.JRAbstractScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 17.33
 * To change this template use File | Settings | File Templates.
 */

public class LogScriplet extends JRAbstractScriptlet {

    @Override
    public void afterColumnInit() {

        System.out.println("After Column Init ");

    }

    @Override
    public void afterDetailEval() throws JRScriptletException {

        System.out.println("After Detail Eval " + getFieldValue("product_id"));

    }

    @Override
    public void afterGroupInit(final String groupName) {

        System.out.println("After Init Group : " + groupName);

    }

    @Override
    public void afterPageInit() {


    }

    @Override
    public void afterReportInit() {

        System.out.println("After Report Init");

    }

    @Override
    public void beforeColumnInit() {

        System.out.println("Before Column Init");

    }

    @Override
    public void beforeDetailEval() {

        // TODO Auto-generated method stub

    }

    @Override
    public void beforeGroupInit(final String groupName) {

        // TODO Auto-generated method stub

    }

    @Override
    public void beforePageInit() {

        // TODO Auto-generated method stub

    }

    @Override
    public void beforeReportInit() throws JRScriptletException {

        System.out.println("beforeReportInit : Param Value Passed" + getParameterValue("productid"));

    }

}