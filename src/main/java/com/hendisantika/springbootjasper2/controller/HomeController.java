package com.hendisantika.springbootjasper2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 17.34
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class HomeController {
    @GetMapping({"/", "/index"})
    public String home() {

        return "index";
    }
}